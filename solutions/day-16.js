const pattern = [0, 1, 0, -1];

function calculateFFTPhase(input) {
  return input.map(
    (_, i) =>
      Math.abs(
        input.reduce((t, v, j) => {
          if (j < i) return t + pattern[0] * v;
          const factor =
            pattern[Math.floor((j + 1) / (i + 1)) % pattern.length];
          return t + factor * v;
        }, 0)
      ) % 10
  );
}

function repeat(list, times) {
  return Array.from({ length: list.length * times }).map(
    (_, i) => list[i % list.length]
  );
}

function fastFFTPhase(list) {
  let r = Array.from(list);
  for (let i = r.length - 2; i >= 0; i--) {
    r[i] = Math.abs(list[i] + r[i + 1]) % 10;
  }
  return r;
}

module.exports = {
  part1: (inputLines, phases = 100) => {
    let list = inputLines[0].split('').map(v => parseInt(v, 10));
    while (phases--) {
      list = calculateFFTPhase(list);
    }
    return list.slice(0, 8).join('');
  },
  part2: (inputLines, phases = 100) => {
    let inputList = inputLines[0].split('').map(v => parseInt(v, 10));
    let offset = parseInt(inputLines[0].slice(0, 7), 10);
    let list = repeat(
      inputList,
      Math.ceil((inputList.length * 10000 - offset) / inputList.length)
    ).slice(offset % inputList.length);

    while (phases--) {
      list = fastFFTPhase(list);
    }

    return list.slice(0, 8).join('');
  },
};
