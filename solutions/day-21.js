const { runProgram } = require('./intcode-computer');

const WALK = [...'WALK'.split('').map(v => v.charCodeAt(0)), 10];
const RUN = [...'RUN'.split('').map(v => v.charCodeAt(0)), 10];

function toAscii(inputList) {
  const o = [];
  for (const input of inputList) {
    o.push(...input.split('').map(v => v.charCodeAt(0)));
    o.push(10);
  }
  return o;
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const { output } = runProgram(intCodeList, [
      ...toAscii(['NOT C J', 'AND D J', 'NOT A T', 'OR T J']),
      ...WALK,
    ]);
    return output[output.length - 1];
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const { output } = runProgram(intCodeList, [
      ...toAscii([
        'OR B J',
        'AND C J',
        'NOT J J',
        'AND D J',
        'AND H J',
        'NOT A T',
        'OR T J',
      ]),
      ...RUN,
    ]);
    return output[output.length - 1];
  },
};
