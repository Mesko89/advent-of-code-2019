const { part1, part2 } = require('./day-01');

describe('Day 01 - Part 1', () => {
  const tests = [
    { input: ['12'], expectedOutput: 2 },
    { input: ['14'], expectedOutput: 2 },
    { input: ['1969'], expectedOutput: 654 },
    { input: ['100756'], expectedOutput: 33583 },
    { input: ['12', '14', '1969', '100756'], expectedOutput: 34241 },
  ];
  for (const test of tests) {
    it(`calculates fuel requirements for modules "${test.input}"`, () => {
      expect(part1(test.input)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 01 - Part 2', () => {
  const tests = [
    { input: ['14'], expectedOutput: 2 },
    { input: ['1969'], expectedOutput: 966 },
    { input: ['100756'], expectedOutput: 50346 },
  ];

  for (const test of tests) {
    it(`calculates real fuel requirements for modules "${test.input}"`, () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
