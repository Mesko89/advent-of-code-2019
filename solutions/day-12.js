const lcm = require('lcm');

function parseMoons(inputLines) {
  return inputLines.map(line => {
    const [_, x, y, z] = line
      .match(/x=(-?\d+), y=(-?\d+), z=(-?\d+)/)
      .map(v => parseInt(v));
    return { position: [x, y, z], velocity: [0, 0, 0] };
  });
}

function step(moons) {
  moons = moons.map(({ position: moonPosition, velocity }, moonIndex) => {
    return {
      position: moonPosition,
      velocity: moons.reduce((calculatedVelocity, { position }, i) => {
        if (i === moonIndex) return calculatedVelocity;
        return calculatedVelocity.map(
          (vel, i) =>
            vel +
            (moonPosition[i] === position[i]
              ? 0
              : moonPosition[i] < position[i]
              ? 1
              : -1)
        );
      }, velocity),
    };
  });
  return moons.map(({ position, velocity }) => ({
    position: position.map((v, i) => v + velocity[i]),
    velocity,
  }));
}

function simulate(moons, steps) {
  while (steps--) {
    moons = step(moons);
  }
  return moons;
}

function toAbsSum(total, value) {
  if (!total) total = 0;
  return total + Math.abs(value);
}

function getTotalEnergy(moons) {
  return moons.reduce((total, { position, velocity }) => {
    return total + position.reduce(toAbsSum, 0) * velocity.reduce(toAbsSum, 0);
  }, 0);
}

function getOrbitFrequencies(moons) {
  const visitedStates = [new Set(), new Set(), new Set()];
  const frequencies = [-1, -1, -1];

  let step = 0;
  do {
    for (let d = 0; d < visitedStates.length; d++) {
      if (frequencies[d] >= 0) continue;
      const key = JSON.stringify(
        moons.map(({ position, velocity }) => [position[d], velocity[d]])
      );
      if (visitedStates[d].has(key)) {
        frequencies[d] = step;
      } else {
        visitedStates[d].add(key);
      }
    }
    moons = simulate(moons, 1);
    step++;
  } while (frequencies.some(v => v < 0));
  return frequencies;
}

module.exports = {
  part1: (inputLines, steps = 1000) => {
    const moons = parseMoons(inputLines);
    const simulationResults = simulate(moons, steps);
    return getTotalEnergy(simulationResults);
  },
  part2: inputLines => {
    const moons = parseMoons(inputLines);
    const orbitFrequencies = getOrbitFrequencies(moons);
    return orbitFrequencies
      .slice(1)
      .reduce((v, f) => lcm(v, f), orbitFrequencies[0]);
  },
};
