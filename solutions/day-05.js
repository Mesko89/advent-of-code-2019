const { runProgram } = require('./intcode-computer');

module.exports = {
  part1: (inputLines, fullOutput = false) => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const { output } = runProgram(intCodeList, [1]);
    if (fullOutput) return output;
    return output[output.length - 1];
  },
  part2: (inputLines, input = [5], fullOutput = false) => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const { output } = runProgram(intCodeList, input);
    if (fullOutput) return output;
    return output[output.length - 1];
  },
};
