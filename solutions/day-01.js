function getFuelRequirementForMass(mass) {
  return Math.floor(mass / 3) - 2;
}

function getFuelRequirementForMassWithFuel(mass) {
  const fuelRequirement = getFuelRequirementForMass(mass);
  return fuelRequirement > 0
    ? fuelRequirement + getFuelRequirementForMassWithFuel(fuelRequirement)
    : 0;
}

module.exports = {
  part1: inputLines =>
    inputLines
      .map(v => parseInt(v, 10))
      .map(mass => getFuelRequirementForMass(mass))
      .reduce((total, mass) => total + mass, 0),
  part2: inputLines =>
    inputLines
      .map(v => parseInt(v, 10))
      .map(mass => getFuelRequirementForMassWithFuel(mass))
      .reduce((total, mass) => total + mass, 0),
};
