const BUG = '#';
const EMPTY = '.';

function calculateBiodiversity(field) {
  return field
    .join('')
    .split('')
    .reduce(
      (total, v, power) => (v !== '#' ? total : total + Math.pow(2, power)),
      0
    );
}

function get(field, x, y, level) {
  if (level === undefined) {
    if (x < 0 || y < 0) return EMPTY;
    if (x >= field[0].length || y >= field.length) return EMPTY;
    return field[y][x];
  }
  if (level < 0 || level >= field.length) return EMPTY;
  return field[level][y][x];
}

function getNeighbors(x, y, level) {
  let neighbors = [
    [x + 1, y],
    [x - 1, y],
    [x, y + 1],
    [x, y - 1],
  ];
  if (level === undefined) return neighbors;
  neighbors.forEach(n => n.push(level));
  neighbors = neighbors.reduce((neighbors, [nX, nY, nLevel]) => {
    if (nX < 0) neighbors.push([1, 2, nLevel + 1]);
    else if (nY < 0) neighbors.push([2, 1, nLevel + 1]);
    else if (nX > 4) neighbors.push([3, 2, nLevel + 1]);
    else if (nY > 4) neighbors.push([2, 3, nLevel + 1]);
    else if (nX === 2 && nY === 2) {
      if (y === 2) {
        neighbors.push(
          [x === 1 ? 0 : 4, 0, level - 1],
          [x === 1 ? 0 : 4, 1, level - 1],
          [x === 1 ? 0 : 4, 2, level - 1],
          [x === 1 ? 0 : 4, 3, level - 1],
          [x === 1 ? 0 : 4, 4, level - 1]
        );
      } else if (x === 2) {
        neighbors.push(
          [0, y === 1 ? 0 : 4, level - 1],
          [1, y === 1 ? 0 : 4, level - 1],
          [2, y === 1 ? 0 : 4, level - 1],
          [3, y === 1 ? 0 : 4, level - 1],
          [4, y === 1 ? 0 : 4, level - 1]
        );
      }
    } else {
      neighbors.push([nX, nY, nLevel]);
    }
    return neighbors;
  }, []);
  return neighbors;
}

const rules = {
  [BUG](field, x, y, level) {
    const neighbors = getNeighbors(x, y, level);
    const totalBugs = neighbors.filter(
      ([x, y, level]) => get(field, x, y, level) === BUG
    ).length;
    return totalBugs === 1 ? BUG : EMPTY;
  },
  [EMPTY](field, x, y, level) {
    const neighbors = getNeighbors(x, y, level);
    const totalBugs = neighbors.filter(
      ([x, y, level]) => get(field, x, y, level) === BUG
    ).length;
    return totalBugs > 0 && totalBugs <= 2 ? BUG : EMPTY;
  },
};

function emptyField() {
  return ['.....', '.....', '.....', '.....', '.....'];
}

function simulateMinute(field) {
  const newField = [];
  for (let y = 0; y < field.length; y++) {
    let line = '';
    for (let x = 0; x < field.length; x++) {
      line += rules[get(field, x, y)](field, x, y);
    }
    newField.push(line);
  }
  return newField;
}

function simulatePlutonianMinute(layers) {
  const newLayers = [];
  for (let level = 0; level < layers.length; level++) {
    const field = layers[level];
    newLayers[level] = [...field];
    for (let y = 0; y < field.length; y++) {
      let line = '';
      for (let x = 0; x < field.length; x++) {
        if (x === 2 && y === 2) line += '.';
        else line += rules[get(layers, x, y, level)](layers, x, y, level);
      }
      newLayers[level][y] = line;
    }
  }

  // Include new fields if edge fields contain bugs
  if (calculateBiodiversity(newLayers[0]) !== 0) {
    newLayers.unshift(emptyField());
  }
  if (calculateBiodiversity(newLayers[newLayers.length - 1]) !== 0) {
    newLayers.push(emptyField());
  }

  return newLayers;
}

module.exports = {
  calculateBiodiversity,
  simulateMinute,
  getNeighbors,
  part1: field => {
    const key = field => field.join('');
    const seenStates = new Set();
    let state = key(field);
    do {
      seenStates.add(state);
      field = simulateMinute(field);
      state = key(field);
    } while (!seenStates.has(state));
    return calculateBiodiversity(field);
  },
  part2: (inputLines, minutes = 200) => {
    let layers = [emptyField(), inputLines, emptyField()];
    while (minutes--) {
      layers = simulatePlutonianMinute(layers);
    }
    return layers.reduce(
      (t, layer) =>
        t +
        layer.reduce(
          (t, line) => t + line.split('').filter(v => v === BUG).length,
          0
        ),
      0
    );
  },
};
