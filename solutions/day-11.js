const { runProgram } = require('./intcode-computer');

const Direction = {
  Up: { x: 0, y: -1 },
  Right: { x: 1, y: 0 },
  Down: { x: 0, y: 1 },
  Left: { x: -1, y: 0 },
};
const LEFT = 0;
const RIGHT = 1;

function turn(currentDirection, leftOrRight) {
  if (leftOrRight === LEFT) {
    if (currentDirection === Direction.Up) return Direction.Left;
    if (currentDirection === Direction.Right) return Direction.Up;
    if (currentDirection === Direction.Down) return Direction.Right;
    if (currentDirection === Direction.Left) return Direction.Down;
  } else {
    if (currentDirection === Direction.Up) return Direction.Right;
    if (currentDirection === Direction.Right) return Direction.Down;
    if (currentDirection === Direction.Down) return Direction.Left;
    if (currentDirection === Direction.Left) return Direction.Up;
  }
}

function stringField(field, bounds) {
  let output = '\n';
  for (let y = bounds.minY; y <= bounds.maxY; y++) {
    for (let x = bounds.minX; x <= bounds.maxX; x++) {
      const key = `${x},${y}`;
      if (!(key in field) || field[key] === 0) output += ' ';
      if (field[key] === 1) output += '█';
    }
    output += '\n';
  }
  return output;
}

function createPanel() {
  const field = {};
  const bounds = {
    minX: Number.MAX_VALUE,
    minY: Number.MAX_VALUE,
    maxX: Number.MIN_VALUE,
    maxY: Number.MIN_VALUE,
  };
  let position = { x: 0, y: 0 };
  let direction = Direction.Up;
  return {
    get key() {
      return `${position.x},${position.y}`;
    },
    paint(color) {
      field[this.key] = color;
    },
    move() {
      position.x += direction.x;
      position.y += direction.y;
      if (bounds.minX > position.x) bounds.minX = position.x;
      if (bounds.minY > position.y) bounds.minY = position.y;
      if (bounds.maxX < position.x) bounds.maxX = position.x;
      if (bounds.maxY < position.y) bounds.maxY = position.y;
    },
    rotate(leftOrRight) {
      direction = turn(direction, leftOrRight);
    },
    currentPaint() {
      return this.key in field ? field[this.key] : 0;
    },
    get field() {
      return field;
    },
    toString() {
      return stringField(field, bounds);
    },
  };
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const panel = createPanel();
    let ctx = runProgram(intCodeList, [panel.currentPaint()]);
    do {
      const [color, rotation] = ctx.output.slice(-2);
      panel.paint(color);
      panel.rotate(rotation);
      panel.move();
      ctx.pushInput(panel.currentPaint());
      ctx = ctx.resume();
    } while (!ctx.isHalted);

    return Object.values(panel.field).length;
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const panel = createPanel();
    let ctx = runProgram(intCodeList, [1]);
    do {
      const [color, rotation] = ctx.output.slice(-2);
      panel.paint(color);
      panel.rotate(rotation);
      panel.move();
      ctx.pushInput(panel.currentPaint());
      ctx = ctx.resume();
    } while (!ctx.isHalted);

    return panel.toString();
  },
};
