const { part1, part2 } = require('./day-02');

describe('Day 02 - Part 1', () => {
  const tests = [
    { input: ['1,0,0,0,99'], expectedOutput: 2 },
    { input: ['2,3,0,3,99'], expectedOutput: 2 },
  ];
  for (const test of tests) {
    it(`calculates value at position 0 for program: "${test.input}"`, () => {
      expect(part1(test.input)).toBe(test.expectedOutput);
    });
  }
});
