const { part1, part2 } = require('./day-12');

describe('Day 12 - Part 1', () => {
  const tests = [
    {
      input: [
        '<x=-1, y=0, z=2>',
        '<x=2, y=-10, z=-7>',
        '<x=4, y=-8, z=8>',
        '<x=3, y=5, z=-1>',
      ],
      steps: 10,
      expectedOutput: 179,
    },
    {
      input: [
        '<x=-8, y=-10, z=0>',
        '<x=5, y=5, z=10>',
        '<x=2, y=-7, z=3>',
        '<x=9, y=-8, z=-3>',
      ],
      steps: 100,
      expectedOutput: 1940,
    },
  ];
  for (const test of tests) {
    it(`returns the total energy in the system for "${test.input}"`, () => {
      expect(part1(test.input, test.steps)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 12 - Part 2', () => {
  const tests = [
    {
      input: [
        '<x=-1, y=0, z=2>',
        '<x=2, y=-10, z=-7>',
        '<x=4, y=-8, z=8>',
        '<x=3, y=5, z=-1>',
      ],
      expectedOutput: 2772,
    },
    {
      input: [
        '<x=-8, y=-10, z=0>',
        '<x=5, y=5, z=10>',
        '<x=2, y=-7, z=3>',
        '<x=9, y=-8, z=-3>',
      ],
      expectedOutput: 4686774924,
    },
  ];
  for (const test of tests) {
    it(`returns number of steps to reach the initial state for "${test.input}"`, () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
