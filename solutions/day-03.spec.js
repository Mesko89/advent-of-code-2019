const { part1, part2 } = require('./day-03');

describe('Day 03 - Part 1', () => {
  const tests = [
    { input: ['R8,U5,L5,D3', 'U7,R6,D4,L4'], expectedOutput: 6 },
    {
      input: [
        'R75,D30,R83,U83,L12,D49,R71,U7,L72',
        'U62,R66,U55,R34,D71,R55,D58,R83',
      ],
      expectedOutput: 159,
    },
    {
      input: [
        'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51',
        'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7',
      ],
      expectedOutput: 135,
    },
  ];
  for (const test of tests) {
    it(`calculates distance between (o) and first intersection for: "${test.input.join(
      ';'
    )}"`, () => {
      expect(part1(test.input)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 03 - Part 2', () => {
  const tests = [
    { input: ['R8,U5,L5,D3', 'U7,R6,D4,L4'], expectedOutput: 30 },
    {
      input: [
        'R75,D30,R83,U83,L12,D49,R71,U7,L72',
        'U62,R66,U55,R34,D71,R55,D58,R83',
      ],
      expectedOutput: 610,
    },
    {
      input: [
        'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51',
        'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7',
      ],
      expectedOutput: 410,
    },
  ];
  for (const test of tests) {
    it(`calculates fewest combined steps the wires must take to reach an intersection for: "${test.input.join(
      ';'
    )}"`, () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
