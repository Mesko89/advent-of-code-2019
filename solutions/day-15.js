const aStar = require('a-star');
const { runProgram, continueFrom } = require('./intcode-computer');

const EMPTY = ' ';
const PATH = '.';
const WALL = '#';
const OXYGEN = 'O';
const Move = [
  { x: 0, y: -1 }, // north
  { x: 0, y: 1 }, // south
  { x: -1, y: 0 }, // west
  { x: 1, y: 0 }, // east
];

function stringField(field, bounds) {
  let output = '\n';
  for (let y = bounds.minY; y <= bounds.maxY; y++) {
    for (let x = bounds.minX; x <= bounds.maxX; x++) {
      const key = `${x},${y}`;
      if (!(key in field)) output += EMPTY;
      else output += field[key].tile;
    }
    output += '\n';
  }
  return output;
}

function createField(intCodeApp) {
  const startPosition = { x: 0, y: 0 };
  let oxygenPosition = null;
  const field = {
    '0,0': {
      position: { x: 0, y: 0 },
      tile: 'S',
      ctx: runProgram(intCodeApp, []).ctx,
    },
  };
  const bounds = { minX: 0, minY: 0, maxX: 0, maxY: 0 };

  function updateBounds(pos) {
    if (pos.x < bounds.minX) bounds.minX = pos.x;
    if (pos.x > bounds.maxX) bounds.maxX = pos.x;
    if (pos.y < bounds.minY) bounds.minY = pos.y;
    if (pos.y > bounds.maxY) bounds.maxY = pos.y;
  }

  const toCheck = Move.map((_, direction) => ({
    field: field['0,0'],
    direction,
  }));
  while (toCheck.length) {
    const {
      field: { position, ctx: appCtx },
      direction,
    } = toCheck.pop();
    const newPosition = {
      x: position.x + Move[direction].x,
      y: position.y + Move[direction].y,
    };
    const key = `${newPosition.x},${newPosition.y}`;
    if (key in field) continue;

    const { output, ctx } = continueFrom(appCtx, [direction + 1]);
    const outputValue = output[output.length - 1];

    updateBounds(newPosition);

    if (outputValue === 0) {
      field[key] = { tile: WALL, position: newPosition };
    } else {
      if (outputValue === 2) {
        oxygenPosition = newPosition;
      }
      field[key] = {
        tile: outputValue === 2 ? OXYGEN : PATH,
        position: newPosition,
        ctx,
      };
      toCheck.push(
        ...Move.map((_, direction) => ({
          field: field[key],
          direction,
        }))
      );
    }
  }
  return {
    field,
    oxygenPosition,
    startPosition,
    bounds,
  };
}

function findShortest(field, startPosition, oxygenPosition) {
  function manhattanDistance(p1, p2) {
    return Math.abs(p2.x - p1.x) + Math.abs(p2.y - p1.y);
  }

  const { path } = aStar({
    start: startPosition,
    isEnd: ({ x, y }) => x === oxygenPosition.x && y === oxygenPosition.y,
    neighbor: ({ x, y }) =>
      Move.map(pos => ({ x: x + pos.x, y: y + pos.y })).filter(
        ({ x, y }) => field[`${x},${y}`].tile !== WALL
      ),
    distance: () => 1,
    heuristic: pos => manhattanDistance(pos, oxygenPosition),
    hash: ({ x, y }) => `${x},${y}`,
  });
  return path;
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const { field, startPosition, oxygenPosition } = createField(intCodeList);
    const shortestPath = findShortest(field, startPosition, oxygenPosition);
    return shortestPath.length - 1;
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const { field, oxygenPosition, bounds } = createField(intCodeList);
    let lastFilledTiles = [oxygenPosition];
    let minutes = -1;
    while (lastFilledTiles.length) {
      minutes++;
      const filledTiles = [];
      for (let { x, y } of lastFilledTiles) {
        filledTiles.push(
          ...Move.map(({ x: dx, y: dy }) => {
            return { x: x + dx, y: y + dy };
          }).filter(({ x, y }) => {
            const key = `${x},${y}`;
            if (field[key].tile === PATH) {
              field[key].tile = OXYGEN;
              return true;
            }
            return false;
          })
        );
      }
      lastFilledTiles = filledTiles;
    }
    return minutes;
  },
};
