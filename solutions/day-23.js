const { runProgram } = require('./intcode-computer');
function startComputers(totalComputers, program) {
  return Array.from({ length: totalComputers }).map((_, i) =>
    runProgram(program, [i])
  );
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const computers = startComputers(50, intCodeList);
    let currentComputer = 0;
    let inputs = computers.map(() => []);
    do {
      const computer = computers[currentComputer];
      if (inputs[currentComputer].length === 0) {
        computer.pushInput(-1);
      } else {
        inputs[currentComputer].forEach(v => computer.pushInput(v));
        inputs[currentComputer] = [];
      }
      const { output } = computer.resume(true);
      for (let i = 0; i < output.length; i += 3) {
        const [location, x, y] = output.slice(i, i + 3);
        if (location === 255) return y;
        inputs[location].push(x, y);
      }
      currentComputer = (currentComputer + 1) % computers.length;
    } while (true);
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const computers = startComputers(50, intCodeList);
    let currentComputer = 0;
    let inputs = computers.map(() => []);
    let lastNATPacket = [];
    let lastYPushedFromNAT = null;
    do {
      const computer = computers[currentComputer];
      if (inputs[currentComputer].length === 0) {
        computer.pushInput(-1);
      } else {
        inputs[currentComputer].forEach(v => computer.pushInput(v));
        inputs[currentComputer] = [];
      }
      const { output } = computer.resume(true);
      for (let i = 0; i < output.length; i += 3) {
        const [location, x, y] = output.slice(i, i + 3);
        if (location === 255) {
          lastNATPacket = [x, y];
        } else {
          inputs[location].push(x, y);
        }
      }

      const areIdle = inputs.every(input => input.length === 0);
      if (areIdle) {
        if (lastYPushedFromNAT === lastNATPacket[1]) return lastYPushedFromNAT;
        lastYPushedFromNAT = lastNATPacket[1];
        inputs[0].push(...lastNATPacket);
      }

      currentComputer = (currentComputer + 1) % computers.length;
    } while (true);
  },
};
