const { runProgram } = require('./intcode-computer');
const EMPTY = ' ';
const WALL = '█';
const BLOCK = '#';
const PADDLE = '-';
const BALL = '⏺';

const Fields = {
  0: EMPTY,
  1: WALL,
  2: BLOCK,
  3: PADDLE,
  4: BALL,
};

function stringField(field, bounds, key) {
  let output = '\n';
  for (let y = bounds.minY; y <= bounds.maxY; y++) {
    for (let x = bounds.minX; x <= bounds.maxX; x++) {
      if (!(key(x, y) in field)) output += EMPTY;
      else output += field[key(x, y)];
    }
    output += '\n';
  }
  return output;
}

function createEmptyGame() {
  const field = {};
  const bounds = {
    minX: Number.MAX_VALUE,
    minY: Number.MAX_VALUE,
    maxX: Number.MIN_VALUE,
    maxY: Number.MIN_VALUE,
  };
  let score = 0;
  let ballPosition = { x: 0, y: 0 };
  let paddlePosition = { x: 0, y: 0 };
  return {
    get score() {
      return score;
    },
    get ballPosition() {
      return ballPosition;
    },
    get paddlePosition() {
      return paddlePosition;
    },
    key(x, y) {
      return `${x},${y}`;
    },
    set(x, y, tileId) {
      if (x === -1 && y === 0) {
        score = tileId;
        return;
      } else {
        const tile = Fields[tileId];
        field[this.key(x, y)] = tile;
        if (tile === BALL) {
          ballPosition = { x, y };
        } else if (tile === PADDLE) {
          paddlePosition = { x, y };
        }
        if (bounds.minX > x) bounds.minX = x;
        if (bounds.minY > y) bounds.minY = y;
        if (bounds.maxX < x) bounds.maxX = x;
        if (bounds.maxY < y) bounds.maxY = y;
      }
    },
    get field() {
      return field;
    },
    toString() {
      return stringField(field, bounds, this.key);
    },
  };
}

function updateGame(game, output) {
  for (let i = 0; i < output.length; i += 3) {
    const [x, y, tileId] = output.slice(i, i + 3);
    game.set(x, y, tileId);
  }
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const game = createEmptyGame();
    let ctx = runProgram(intCodeList, []);
    for (let i = 0; i < ctx.output.length; i += 3) {
      const [x, y, tileId] = ctx.output.slice(i, i + 3);
      game.set(x, y, tileId);
    }
    return Object.values(game.field).filter(v => v === BLOCK).length;
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const game = createEmptyGame();
    intCodeList[0] = 2;
    let ctx = runProgram(intCodeList, []);
    updateGame(game, ctx.output);
    do {
      if (game.ballPosition.x < game.paddlePosition.x) {
        ctx.pushInput(-1);
      } else if (game.ballPosition.x > game.paddlePosition.x) {
        ctx.pushInput(1);
      } else {
        ctx.pushInput(0);
      }
      ctx = ctx.resume();
      updateGame(game, ctx.output);
    } while (!ctx.isHalted);

    return game.score;
  },
};
