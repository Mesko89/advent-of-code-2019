const {
  BigInt: { modInv, modPow },
} = require('./utils');

function parseCommands(lines) {
  return lines.map(line => {
    if (line === 'deal into new stack') {
      return ['reverse'];
    } else if (/^cut/.test(line)) {
      return ['cut', parseInt(line.replace('cut ', ''), 10)];
    } else if (/^deal with increment/.test(line)) {
      return ['deal', parseInt(line.replace('deal with increment ', ''), 10)];
    }
  });
}

const commands = {
  reverse(list) {
    return list.reverse();
  },
  cut(list, n) {
    if (n >= 0) {
      return [...list.slice(n), ...list.slice(0, n)];
    } else {
      return [...list.slice(n), ...list.slice(0, list.length + n)];
    }
  },
  deal(list, n) {
    let p = 0;
    const newList = [...list];
    for (const v of list) {
      newList[p] = v;
      p = (p + n) % list.length;
    }
    return newList;
  },
};

const diffCalcCommands = {
  reverse([offset, mul], deckSize) {
    const newMul = -mul % deckSize;
    const newOffset = (offset + newMul) % deckSize;
    return [newOffset, newMul];
  },
  cut([offset, mul], n, deckSize) {
    return [(offset + BigInt(n) * mul) % deckSize, mul];
  },
  deal([offset, mul], n, deckSize) {
    return [offset, (mul * modInv(BigInt(n), deckSize)) % deckSize];
  },
};

module.exports = {
  part1: (inputLines, { deckSize = 10007, returnWhole = false } = {}) => {
    const commandList = parseCommands(inputLines);
    let list = Array.from({ length: deckSize }).map((_, i) => i);
    for (const [commandName, ...args] of commandList) {
      list = commands[commandName](list, ...args);
    }
    return returnWhole ? list.join(' ') : list.indexOf(2019);
  },
  part2: inputLines => {
    const deckSize = BigInt(119315717514047);
    const iterations = BigInt(101741582076661);
    const commandList = parseCommands(inputLines);

    function findOffsetAndMultiplier() {
      let diffs = [BigInt(0), BigInt(1)];
      for (const [commandName, ...args] of commandList) {
        diffs = diffCalcCommands[commandName](diffs, ...args, deckSize);
      }
      return diffs;
    }

    let [offset, multiplier] = findOffsetAndMultiplier();

    const finalMultiplier = modPow(multiplier, iterations, deckSize);
    offset *=
      (BigInt(1) - finalMultiplier) *
      modInv((BigInt(1) - multiplier) % deckSize, deckSize);
    offset %= deckSize;

    return (offset + finalMultiplier * BigInt(2020)) % deckSize;
  },
};
