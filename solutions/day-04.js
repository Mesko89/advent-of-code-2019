const repeatDigitsRx = /(\d)\1+/;
const goodPasswordRules = [
  password => repeatDigitsRx.test(password),
  password =>
    password ===
    password
      .split('')
      .sort()
      .join(''),
];

const betterPasswordRules = [
  ...goodPasswordRules,
  password => {
    const digits = password.split('').reduce((digits, d) => {
      if (!(d in digits)) digits[d] = 0;
      digits[d]++;
      return digits;
    }, {});
    return Object.values(digits).some(d => d === 2);
  },
];

function getPasswords(from, to, rules) {
  const passwords = [];
  const isPasswordGood = password => rules.every(v => v(password));
  for (let i = from; i < to; i++) {
    const password = i.toString();
    if (isPasswordGood(password)) {
      passwords.push(password);
    }
  }
  return passwords;
}

module.exports = {
  part1: inputLines => {
    const [from, to] = inputLines[0].split('-').map(v => parseInt(v, 10));
    const goodPasswords = getPasswords(from, to, goodPasswordRules);
    return goodPasswords.length;
  },
  part2: inputLines => {
    const [from, to] = inputLines[0].split('-').map(v => parseInt(v, 10));
    const goodPasswords = getPasswords(from, to, betterPasswordRules);
    return goodPasswords.length;
  },
};
