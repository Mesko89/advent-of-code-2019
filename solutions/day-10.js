function parseMap(lines) {
  return lines.map(l => l.split(''));
}

function isVisible(position, from, map) {
  if (position.x === from.x && position.y === from.y) return false;
  const bounds = {
    minX: from.x < position.x ? from.x : position.x,
    minY: from.y < position.y ? from.y : position.y,
    maxX: from.x > position.x ? from.x : position.x,
    maxY: from.y > position.y ? from.y : position.y,
  };
  const ratio = (position.x - from.x) / (position.y - from.y);
  for (let y = bounds.minY; y <= bounds.maxY; y++) {
    for (let x = bounds.minX; x <= bounds.maxX; x++) {
      if (
        (x === position.x && y === position.y) ||
        (x === from.x && y === from.y)
      )
        continue;
      if ((x - from.x) / (y - from.y) === ratio && map[y][x] === '#') {
        return false;
      }
    }
  }
  return true;
}

function getVisibleFrom(position, map) {
  const visible = [];
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x] !== '#') continue;
      if (isVisible({ x, y }, position, map)) visible.push({ x, y });
    }
  }
  return visible;
}

function scanLocations(map) {
  const scanResults = {};
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x] !== '#') continue;
      const position = { x, y };
      const asteroidsVisible = getVisibleFrom(position, map);
      scanResults[`${x},${y}`] = asteroidsVisible;
    }
  }
  return scanResults;
}

function angle(from, to) {
  const x = to.x - from.x;
  const y = to.y - from.y;
  let angle =
    y === 0
      ? x < 0
        ? -Math.PI
        : 0
      : 2 * Math.atan(y / (x + Math.sqrt(x * x + y * y)));

  angle += (1 / 2) * Math.PI;

  return angle < 0 ? angle + 2 * Math.PI : angle;
}

module.exports = {
  part1: inputLines => {
    const map = parseMap(inputLines);
    const scanResults = scanLocations(map);
    return Math.max(...Object.values(scanResults).map(v => v.length));
  },
  part2: inputLines => {
    const map = parseMap(inputLines);
    const scanResults = scanLocations(map);
    const mostVisibleAsteroids = Math.max(
      ...Object.values(scanResults).map(v => v.length)
    );
    const [location, visible] = Object.entries(scanResults).find(
      ([_, values]) => values.length === mostVisibleAsteroids
    );
    const position = { x: location.split(',')[0], y: location.split(',')[1] };
    visible.sort((a, b) => angle(position, a) - angle(position, b));

    const twoHundredth = visible[199];
    return twoHundredth.x * 100 + twoHundredth.y;
  },
};
