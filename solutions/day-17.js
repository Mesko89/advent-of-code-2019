const { runProgram } = require('./intcode-computer');

const SCAFFOLD = '#';
const EMPTY = '.';
const ROBOT = '^';

function createField(intCodeApp) {
  const { output } = runProgram(intCodeApp, []);
  const field = output.map(v => String.fromCharCode(v)).join('');

  const width = field.indexOf(String.fromCharCode(10));
  const height = (output.length - 1) / width;

  return {
    field,
    width,
    height,
    get(x, y) {
      if (x > width || x < 0) return EMPTY;
      if (y > height || y < 0) return EMPTY;
      return field.charAt(y * (width + 1) + x);
    },
  };
}

function getNeighbors(x, y) {
  return [
    [x + 1, y],
    [x - 1, y],
    [x, y + 1],
    [x, y - 1],
  ];
}

function getSumOfAlignmentParameters({ get, width, height }) {
  let sum = 0;
  for (let x = 1; x < width - 1; x++) {
    for (let y = 1; y < height - 1; y++) {
      const tile = get(x, y);
      if (tile !== SCAFFOLD) continue;
      const isIntersection = getNeighbors(x, y).every(
        ([x, y]) => get(x, y) === SCAFFOLD
      );
      if (isIntersection) {
        sum += x * y;
      }
    }
  }
  return sum;
}

function getRobotPosition({ width, height, get }) {
  for (let x = 1; x < width - 1; x++) {
    for (let y = 1; y < height - 1; y++) {
      const tile = get(x, y);
      if (tile === ROBOT) {
        return { x, y };
      }
    }
  }
}

const TurnLeft = {
  '0,-1': { x: -1, y: 0 },
  '-1,0': { x: 0, y: 1 },
  '0,1': { x: 1, y: 0 },
  '1,0': { x: 0, y: -1 },
};
const TurnRight = {
  '0,-1': { x: 1, y: 0 },
  '-1,0': { x: 0, y: -1 },
  '0,1': { x: -1, y: 0 },
  '1,0': { x: 0, y: 1 },
};

function getListOfCommands(field) {
  let position = getRobotPosition(field);
  let direction = { x: 0, y: -1 };
  let commandStack = [];
  const canGoForward = () =>
    field.get(position.x + direction.x, position.y + direction.y) === SCAFFOLD;
  const canTurnRight = () =>
    field.get(
      position.x + TurnRight[`${direction.x},${direction.y}`].x,
      position.y + TurnRight[`${direction.x},${direction.y}`].y
    ) === SCAFFOLD;
  const canTurnLeft = () =>
    field.get(
      position.x + TurnLeft[`${direction.x},${direction.y}`].x,
      position.y + TurnLeft[`${direction.x},${direction.y}`].y
    ) === SCAFFOLD;
  while (true) {
    if (canGoForward()) {
      const steps = commandStack.pop();
      commandStack.push(steps + 1);
      position = { x: position.x + direction.x, y: position.y + direction.y };
    } else if (canTurnRight()) {
      direction = TurnRight[`${direction.x},${direction.y}`];
      commandStack.push('R', 0);
    } else if (canTurnLeft()) {
      direction = TurnLeft[`${direction.x},${direction.y}`];
      commandStack.push('L', 0);
    } else {
      break;
    }
  }
  return commandStack;
}

function getOccurrences(list, needle) {
  const indexes = [];
  for (let i = 0; i < list.length - needle.length + 1; i++) {
    let found = true;
    for (let j = 0; j < needle.length; j++) {
      if (list[i + j] !== needle[j]) {
        found = false;
        break;
      }
    }
    if (found) {
      indexes.push(i);
    }
  }
  return indexes;
}

function getAsciiLength(list) {
  return list.join(',').length;
}

function getBestRoutine(list) {
  let value = 0;
  let bestRoutine = [];
  for (let i = 0; i < list.length; i++) {
    if (['A', 'B', 'C'].includes(list[i])) continue;
    for (let l = 2; l < list.length - i; l += 2) {
      let needle = list.slice(i, i + l);
      if (needle.length < 4) continue;
      if (needle.some(v => ['A', 'B', 'C'].includes(v))) break;
      if (getAsciiLength(needle) > 20) break;
      let occurrences = getOccurrences(list, needle);
      let thisValue = getAsciiLength(needle) * occurrences.length;
      if (thisValue > value) {
        value = thisValue;
        bestRoutine = needle;
      } else {
        return bestRoutine;
      }
    }
    return bestRoutine;
  }
}

function commandListReplace(list, routine, name) {
  const indexes = getOccurrences(list, routine);
  const newList = [];
  for (let i = 0; i < list.length; i++) {
    const isInsideRoutine = indexes.some(
      ix => i >= ix && i < ix + routine.length
    );
    if (isInsideRoutine) {
      if (indexes.includes(i)) {
        newList.push(name);
      }
    } else {
      newList.push(list[i]);
    }
  }
  return newList;
}

function compile(list, routineA, routineB, routineC) {
  const toAscii = v =>
    v
      .join(',')
      .split('')
      .map(v => v.charCodeAt(0));
  return toAscii(list)
    .concat(10)
    .concat(...toAscii(routineA), 10)
    .concat(...toAscii(routineB), 10)
    .concat(...toAscii(routineC), 10);
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const field = createField(intCodeList);

    return getSumOfAlignmentParameters(field);
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const field = createField(intCodeList);
    let commandList = getListOfCommands(field);

    const routineA = getBestRoutine(commandList);
    commandList = commandListReplace(commandList, routineA, 'A');
    const routineB = getBestRoutine(commandList);
    commandList = commandListReplace(commandList, routineB, 'B');
    const routineC = getBestRoutine(commandList);
    commandList = commandListReplace(commandList, routineC, 'C');

    const input = compile(commandList, routineA, routineB, routineC);
    input.push('n'.charCodeAt(0), 10);

    const robotIntCodeList = inputLines[0].split(',').map(Number);
    robotIntCodeList[0] = 2;
    const { output } = runProgram(robotIntCodeList, input);

    return output[output.length - 1];
  },
};
