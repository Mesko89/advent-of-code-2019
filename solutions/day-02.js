const operations = {
  1: ({ program, instructionPointer: ip }) => {
    program[program[ip + 3]] =
      (program[program[ip + 1]] || 0) + (program[program[ip + 2]] || 0);
    return ip + 4;
  },
  2: ({ program, instructionPointer: ip }) => {
    program[program[ip + 3]] =
      (program[program[ip + 1]] || 0) * (program[program[ip + 2]] || 0);
    return ip + 4;
  },
  99: () => null,
};

function runProgram([...program]) {
  let instructionPointer = 0;
  do {
    instructionPointer = operations[program[instructionPointer]]({
      program,
      instructionPointer,
    });
  } while (instructionPointer !== null);
  return program;
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    intCodeList[1] = 12;
    intCodeList[2] = 2;
    const endState = runProgram(intCodeList);
    return endState[0];
  },
  part2: (inputLines, expectedValueAt0 = 19690720) => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    for (let noun = 0; noun < 100; noun++) {
      for (let verb = 0; verb < 100; verb++) {
        intCodeList[1] = noun;
        intCodeList[2] = verb;
        const endState = runProgram(intCodeList);
        if (expectedValueAt0 === endState[0]) {
          return 100 * noun + verb;
        }
      }
    }
  },
};
