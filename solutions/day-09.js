const { runProgram } = require('./intcode-computer');

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const { output } = runProgram(intCodeList, [1]);
    return output[output.length - 1];
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const { output } = runProgram(intCodeList, [2]);
    return output[output.length - 1];
  },
};
