const { part1, part2 } = require('./day-08');

describe('Day 08 - Part 1', () => {
  it('returns the number of 1 digits multiplied by the number of 2 digits', () => {
    expect(part1(['123456789012'], 3, 2)).toBe(1);
  });
});

describe('Day 08 - Part 2', () => {
  it('decodes image correctly', () => {
    expect(part2(['0222112222120000'], 2, 2)).toBe('\n █\n█ ');
  });
});
