function getLayers(input, width, height) {
  const layerSize = width * height;
  const totalLayers = input.length / layerSize;
  return Array.from({ length: totalLayers }).map((_, i) =>
    input.slice(layerSize * i, layerSize * (i + 1))
  );
}

function decodeImage(layers) {
  return layers.slice(1).reduce((image, layer) => {
    return image.map((pixel, index) => {
      if (pixel !== 2) return pixel;
      else return layer[index];
    });
  }, layers[0]);
}

module.exports = {
  part1: (inputLines, width = 25, height = 6) => {
    const layers = getLayers(
      inputLines[0].split('').map(v => parseInt(v)),
      width,
      height
    );

    const layerWithMostZeros = layers.reduce(
      ({ layer, totalZeros }, l) => {
        const zeros = l.filter(v => v === 0).length;
        if (totalZeros > zeros) {
          return { layer: l, totalZeros: zeros };
        } else {
          return { layer, totalZeros };
        }
      },
      { layer: '', totalZeros: Number.MAX_VALUE }
    ).layer;
    return (
      layerWithMostZeros.filter(v => v === 1).length *
      layerWithMostZeros.filter(v => v === 2).length
    );
  },
  part2: (inputLines, width = 25, height = 6) => {
    const layers = getLayers(
      inputLines[0].split('').map(v => parseInt(v)),
      width,
      height
    );
    const image = decodeImage(layers);
    const rows = Array.from({ length: height }).map((_, i) =>
      image.slice(i * width, (i + 1) * width)
    );
    return rows
      .reduce((output, row) => `${output}\n${row.join('')}`, '')
      .split('')
      .map(v => (v === '\n' ? v : v === '1' ? '█' : ' '))
      .join('');
  },
};
