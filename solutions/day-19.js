const { runProgram } = require('./intcode-computer');

const Codes = {
  Stationary: 0,
  Pulled: 1,
};

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);

    function check(x, y) {
      const { output } = runProgram(intCodeList, [x, y]);
      return output.slice(-1)[0];
    }

    let output = '\n';
    let totalPulled = 0;
    for (let y = 0; y < 50; y++) {
      for (let x = 0; x < 50; x++) {
        const r = check(x, y);
        if (r === Codes.Pulled) {
          totalPulled++;
        }
        output += r === Codes.Pulled ? '#' : '.';
      }
      output += '\n';
    }
    return totalPulled;
  },
  part2: (inputLines, shipSize = { width: 100, height: 100 }) => {
    const intCodeList = inputLines[0].split(',').map(Number);
    const grid = {};
    const key = (x, y) => `${x},${y}`;

    function check(x, y) {
      const k = key(x, y);
      if (k in grid) return grid[k];
      const { output } = runProgram(intCodeList, [x, y]);
      const r = output.slice(-1)[0];
      grid[k] = r;
      return r;
    }

    function findInterval(line, from = 0) {
      const interval = { from: null, to: null };
      let x = from;
      let r = check(x, line);
      while (r !== Codes.Pulled) {
        x++;
        r = check(x, line);
      }
      interval.from = x;
      while (r !== Codes.Stationary) {
        x++;
        r = check(x, line);
      }
      interval.to = x;
      return interval;
    }

    function checkForShip(fromX, fromY) {
      for (let y = fromY; y < fromY + shipSize.height; y++) {
        let r = check(fromX, y);
        if (r === Codes.Stationary) return false;
      }
      for (let x = fromX; x < fromX + shipSize.width; x++) {
        if (check(x, fromY) === Codes.Stationary) return false;
      }
      return true;
    }

    let xInterval = findInterval(5);
    let y = 900;
    do {
      xInterval = findInterval(y, xInterval.from);
      if (xInterval.to - xInterval.from >= shipSize.width) {
        for (let x = xInterval.from; x < xInterval.to; x++) {
          if (checkForShip(x, y)) {
            return 10000 * x + y;
          }
        }
      }
    } while (y++ < 1e6);
    return undefined;
  },
};
