const { part1, part2 } = require('./day-06');

describe('Day 06 - Part 1', () => {
  const tests = [
    {
      input: ['A)B', 'B)C', 'B)D'],
      expectedOutput: 5,
    },
    {
      input: [
        'COM)B',
        'B)C',
        'C)D',
        'D)E',
        'E)F',
        'B)G',
        'G)H',
        'D)I',
        'E)J',
        'J)K',
        'K)L',
      ],
      expectedOutput: 42,
    },
  ];
  for (const test of tests) {
    it(`returns correct number of orbits for "${test.input}"`, () => {
      expect(part1(test.input)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 06 - Part 2', () => {
  const tests = [
    {
      input: [
        'COM)B',
        'B)C',
        'C)D',
        'D)E',
        'E)F',
        'B)G',
        'G)H',
        'D)I',
        'E)J',
        'J)K',
        'K)L',
        'K)YOU',
        'I)SAN',
      ],
      expectedOutput: 4,
    },
  ];
  for (const test of tests) {
    it('returns the minimum number of orbital transfers required', () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
