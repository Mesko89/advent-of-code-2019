const { part1, part2 } = require('./day-22');

describe('Day 22 - Part 1', () => {
  const tests = [
    {
      input: [
        'deal with increment 7',
        'deal into new stack',
        'deal into new stack',
      ],
      deckSize: 10,
      expectedOutput: '0 3 6 9 2 5 8 1 4 7',
    },
    {
      input: ['cut 6', 'deal with increment 7', 'deal into new stack'],
      deckSize: 10,
      expectedOutput: '3 0 7 4 1 8 5 2 9 6',
    },
    {
      input: ['deal with increment 7', 'deal with increment 9', 'cut -2'],
      deckSize: 10,
      expectedOutput: '6 3 0 7 4 1 8 5 2 9',
    },
    {
      input: [
        'deal into new stack',
        'cut -2',
        'deal with increment 7',
        'cut 8',
        'cut -4',
        'deal with increment 7',
        'cut 3',
        'deal with increment 9',
        'deal with increment 3',
        'cut -1',
      ],
      deckSize: 10,
      expectedOutput: '9 2 5 8 1 4 7 0 3 6',
    },
  ];
  for (const test of tests) {
    it(`returns correct list of cards for for "${test.input}"`, () => {
      expect(
        part1(test.input, { deckSize: test.deckSize, returnWhole: true })
      ).toBe(test.expectedOutput);
    });
  }
});
