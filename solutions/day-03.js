function getBounds(panel) {
  const [minX, minY, maxX, maxY] = Object.keys(panel)
    .map(v => v.split(',').map(n => parseInt(n, 10)))
    .reduce(
      ([minX, minY, maxX, maxY], [x, y]) => [
        minX > x ? x : minX,
        minY > y ? y : minY,
        maxX < x ? x : maxX,
        maxY < y ? y : maxY,
      ],
      [Number.MAX_VALUE, Number.MAX_VALUE, Number.MIN_VALUE, Number.MIN_VALUE]
    );
  return { minX, minY, maxX, maxY };
}

function printPanel(panel) {
  const bounds = getBounds(panel);
  let output = '';
  for (let y = bounds.maxY + 1; y >= bounds.minY - 1; y--) {
    for (let x = bounds.minX - 1; x <= bounds.maxX + 1; x++) {
      const key = `${x},${y}`;
      if (key === '0,0') {
        output += 'o';
      } else if (!(key in panel)) {
        output += '.';
      } else if (panel[key].length > 1) {
        output += 'X';
      } else {
        output += '+';
      }
    }
    output += '\n';
  }
  console.log(output);
}

function move({ x, y }, direction) {
  switch (direction) {
    case 'U':
      return { x, y: y + 1 };
    case 'R':
      return { x: x + 1, y };
    case 'D':
      return { x, y: y - 1 };
    case 'L':
      return { x: x - 1, y };
  }
}

function getPanel(wires) {
  const panel = { '0,0': [] };
  let wireId = 0;
  for (const wire of wires) {
    let currentStep = 0;
    panel['0,0'].push({ wireId: ++wireId, steps: currentStep });

    let position = { x: 0, y: 0 };
    const steps = wire.split(',');
    for (const step of steps) {
      const [_, direction, numberOfStepsStr] = step.match(/(\w)(\d+)/);
      const totalSteps = parseInt(numberOfStepsStr, 10);
      for (let i = 0; i < totalSteps; i++) {
        currentStep++;
        position = move(position, direction);
        const key = `${position.x},${position.y}`;
        if (!(key in panel)) {
          panel[key] = [];
        }
        if (!panel[key].some(d => d.wireId === wireId))
          panel[key].push({ wireId, steps: currentStep });
      }
    }
  }
  return panel;
}

function getIntersections(panel) {
  const startPosition = { x: 0, y: 0 };
  return Object.entries(panel)
    .filter(([position, value]) => position !== '0,0' && value.length > 1)
    .map(([position, value]) => {
      const [x, y] = position.split(',').map(v => parseInt(v, 10));
      return {
        x,
        y,
        distance: manhattanDistance(startPosition, { x, y }),
        steps: value.map(v => v.steps),
      };
    });
}

function manhattanDistance(p1, p2) {
  return Math.abs(p2.x - p1.x) + Math.abs(p2.y - p1.y);
}

module.exports = {
  part1: inputLines => {
    const panel = getPanel(inputLines);
    const intersections = getIntersections(panel);
    return intersections.reduce(
      (closest, intersection) => {
        if (closest.distance > intersection.distance) {
          return intersection;
        } else {
          return closest;
        }
      },
      { distance: Number.MAX_VALUE }
    ).distance;
  },
  part2: inputLines => {
    const panel = getPanel(inputLines);
    const intersections = getIntersections(panel);
    return intersections.reduce((minSteps, { steps }) => {
      const totalSteps = steps.reduce((a, b) => a + b, 0);
      if (minSteps > totalSteps) return totalSteps;
      return minSteps;
    }, Number.MAX_VALUE);
  },
};
