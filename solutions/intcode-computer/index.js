const MODE_POSITION = '0';
const MODE_IMMEDIATE = '1';
const MODE_RELATIVE = '2';

function runProgram([...program], input, ctx = { output: [] }) {
  let {
    instructionPointer = 0,
    relativeBase = 0,
    output: [...output],
    isHalted,
    isWaitingInput,
  } = ctx;
  input = input.reverse();

  function read(value, mode) {
    if (mode === MODE_POSITION) return program[value] || 0;
    if (mode === MODE_IMMEDIATE) return value;
    if (mode === MODE_RELATIVE) return program[relativeBase + value] || 0;
    throw new Error('Could not read value', { program, value, mode });
  }

  function getAddress({ value, mode }) {
    return mode === MODE_RELATIVE ? relativeBase + value : value;
  }

  const operations = {
    1: {
      totalArgs: 3,
      run: ({ args: [A, B, to] }) => {
        program[getAddress(to)] = read(A.value, A.mode) + read(B.value, B.mode);
      },
    },
    2: {
      totalArgs: 3,
      run: ({ args: [A, B, to] }) => {
        program[getAddress(to)] = read(A.value, A.mode) * read(B.value, B.mode);
      },
    },
    3: {
      totalArgs: 1,
      run: ({ input, args: [to] }) => {
        if (!input.length) throw 'no-input';
        program[getAddress(to)] = input.pop();
      },
    },
    4: {
      totalArgs: 1,
      run: ({ args: [from], output }) => {
        output.push(read(from.value, from.mode));
      },
    },
    5: {
      // jump if true
      totalArgs: 2,
      run: ({ args: [check, jump] }) => {
        const value = read(check.value, check.mode);
        if (value !== 0) return read(jump.value, jump.mode);
      },
    },
    6: {
      // jump if false
      totalArgs: 2,
      run: ({ args: [check, jump] }) => {
        const value = read(check.value, check.mode);
        if (value === 0) return read(jump.value, jump.mode);
      },
    },
    7: {
      // less than
      totalArgs: 3,
      run: ({ args: [checkA, checkB, to] }) => {
        if (read(checkA.value, checkA.mode) < read(checkB.value, checkB.mode)) {
          program[getAddress(to)] = 1;
        } else {
          program[getAddress(to)] = 0;
        }
      },
    },
    8: {
      // equals
      totalArgs: 3,
      run: ({ args: [checkA, checkB, to] }) => {
        if (
          read(checkA.value, checkA.mode) === read(checkB.value, checkB.mode)
        ) {
          program[getAddress(to)] = 1;
        } else {
          program[getAddress(to)] = 0;
        }
      },
    },
    9: {
      totalArgs: 1,
      run: ({ args: [{ value, mode }] }) => {
        relativeBase += read(value, mode);
      },
    },
    99: { totalArgs: 0, run: () => null, isExit: true },
  };

  function parseInstruction(instruction) {
    const opCode = instruction % 100;
    const { totalArgs } = operations[opCode];
    if (totalArgs === 0) return { opCode, length: 1 };
    instruction = instruction.toString().padStart(5, '0');
    const modes = Array.from({ length: totalArgs }).map(
      (_, i) => instruction[2 - i]
    );
    return { opCode, modes, length: modes.length + 1 };
  }

  function run() {
    while (!isHalted) {
      const instruction = parseInstruction(program[instructionPointer]);
      const operation = operations[instruction.opCode];
      if (operation.isExit) {
        isHalted = true;
        break;
      }
      const args = instruction.modes.map((mode, i) => ({
        mode,
        value: program[instructionPointer + i + 1],
      }));
      try {
        const jumpTo = operation.run({
          program,
          instructionPointer,
          args,
          input,
          output,
        });

        if (jumpTo !== undefined) instructionPointer = jumpTo;
        else instructionPointer += instruction.length;
      } catch (ex) {
        if (ex === 'no-input') {
          isWaitingInput = true;
          break;
        } else {
          throw ex;
        }
      }
    }
  }

  run();

  function pushInput(value) {
    input.unshift(value);
  }

  function resume(returnNewOutput = false) {
    const outputLength = output.length;
    run();
    return {
      output: returnNewOutput ? output.slice(outputLength) : output,
      isHalted,
      pushInput,
      resume,
      ctx: {
        program,
        instructionPointer,
        relativeBase,
        output: [...output],
        isHalted,
        isWaitingInput,
      },
    };
  }

  return {
    output,
    isWaitingInput,
    isHalted,
    pushInput,
    resume,
    ctx: {
      program,
      instructionPointer,
      relativeBase,
      output: [...output],
      isHalted,
      isWaitingInput,
    },
  };
}

function continueFrom(ctx, input = []) {
  return runProgram(ctx.program, input, ctx);
}

module.exports = {
  runProgram,
  continueFrom,
};
