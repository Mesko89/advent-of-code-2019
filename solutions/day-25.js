const { runProgram } = require('./intcode-computer');
const readline = require('readline');

function readCommand() {
  const reader = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(res => {
    reader.question(`Command > `, command => {
      res(command);
      reader.close();
    });
  });
}

/**
 * Solution might be different for your input.
 * Use npm `run start -- 25` to play interactive version
 */
function getSolutionInput() {
  return [
    'north',
    'north',
    'take sand',
    'south',
    'west',
    'east',
    'south',
    'south',
    'south',
    'east',
    'west',
    'north',
    'west',
    'take wreath',
    'south',
    'west',
    'east',
    'south',
    'take pointer',
    'east',
    'west',
    'north',
    'north',
    'east',
    'north',
    'west',
    'west',
    'east',
    'south',
    'take planetoid',
    'north',
    'west',
    'south',
    'west',
    'north',
    'exit',
  ].reduce((input, command) => {
    return input.concat(...command.split('').map(v => v.charCodeAt(0)), 10);
  }, []);
}

function outputToString(output) {
  return output.map(v => String.fromCharCode(v)).join('');
}

module.exports = {
  part1: async inputLines => {
    const intCodeList = inputLines[0].split(',').map(Number);
    if (!process.argv.includes('25')) {
      const { output } = runProgram(intCodeList, getSolutionInput());
      return outputToString(output).match(
        /in by typing (\d+) on the keypad/
      )[1];
    }
    const { output, resume, pushInput } = runProgram(intCodeList, []);
    console.log(outputToString(output));
    do {
      const command = await readCommand();
      if (command === 'exit') break;
      command
        .toLowerCase()
        .split('')
        .map(v => v.charCodeAt(0))
        .forEach(v => pushInput(v));
      pushInput(10);
      const { output } = resume(true);
      console.log(outputToString(output));
    } while (true);
  },
  part2: () => {
    return 'Merry christmas! 🎄';
  },
};
