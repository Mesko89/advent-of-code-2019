const { runProgram } = require('./intcode-computer');

function permutations(values) {
  if (values.length === 0) return [[]];
  return values.reduce((allPermutations, value) => {
    return [
      ...allPermutations,
      ...permutations(values.filter(v => v !== value)).map(perms =>
        [value].concat(perms)
      ),
    ];
  }, []);
}

module.exports = {
  part1: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const sequences = permutations([0, 1, 2, 3, 4]);

    const outputs = sequences.map(sequence => {
      let output = 0;
      for (let i = 0; i < sequence.length; i++) {
        const ctx = runProgram(intCodeList, [sequence[i], output]);
        output = ctx.output[0];
      }
      return output;
    });
    return Math.max(...outputs);
  },
  part2: inputLines => {
    const intCodeList = inputLines[0].split(',').map(v => parseInt(v, 10));
    const sequences = permutations([5, 6, 7, 8, 9]);
    // const sequences = [[9, 7, 8, 5, 6]];

    let l = 0;
    const outputs = sequences.map(sequence => {
      let programs = sequence.map((s, i) =>
        i === 0 ? runProgram(intCodeList, [s, 0]) : runProgram(intCodeList, [s])
      );
      programs[1].pushInput(programs[0].output[programs[0].output.length - 1]);
      let i = 1;
      while (programs.some(p => !p.isHalted)) {
        let program = programs[i];
        if (program.isHalted) return program;
        let previousOutputLength = program.output.length;
        programs[i] = program.resume();
        if (previousOutputLength < program.output.length) {
          programs[(i + 1) % sequence.length].pushInput(
            program.output[program.output.length - 1]
          );
        }
        i = (i + 1) % sequence.length;
      }
      const output = programs[programs.length - 1].output;
      return output[output.length - 1];
    });
    return Math.max(...outputs);
  },
};
