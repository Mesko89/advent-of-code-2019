const aStar = require('a-star');
const { manhattanDistance } = require('./utils');

const PATH = '.';
const WALL = '#';

function createMaze(lines) {
  const get = (x, y) => lines[y][x];
  const key = (posOrX, y) =>
    y !== undefined ? `${posOrX},${y}` : `${posOrX.x},${posOrX.y}`;
  const isUppercaseRx = /[A-Z]/;

  function findPath(from, to) {
    function getNeighbors(x, y) {
      const neighbors = [
        { x: x - 1, y },
        { x: x + 1, y },
        { x, y: y - 1 },
        { x, y: y + 1 },
      ];
      return neighbors.filter(({ x, y }) => get(x, y) === PATH);
    }

    const { status, cost, path } = aStar({
      start: from,
      isEnd: ({ x, y }) => x === to.x && y === to.y,
      neighbor: ({ x, y }) => getNeighbors(x, y),
      distance: () => 1,
      heuristic: () => 1,
      hash: ({ x, y }) => `${x},${y}`,
    });

    if (status === 'success') return cost;
    return undefined;
  }

  function checkIfOuter(x, y) {
    return x < 2 || x >= lines[0].length - 2 || y < 2 || y >= lines.length - 2;
  }

  const portals = {};

  for (let y = 0; y < lines.length; y++) {
    for (let x = 0; x < lines[y].length; x++) {
      const tile = get(x, y);
      if (isUppercaseRx.test(tile)) {
        if (x + 1 < lines[y].length && isUppercaseRx.test(get(x + 1, y))) {
          const portalCode =
            tile + get(x + 1, y) + (checkIfOuter(x, y) ? '-' : '+');
          if (x + 2 < lines[y].length && get(x + 2, y) === PATH) {
            portals[portalCode] = { x: x + 2, y };
          } else if (get(x - 1, y) === PATH) {
            portals[portalCode] = { x: x - 1, y };
          }
        } else if (y + 1 < lines.length && isUppercaseRx.test(get(x, y + 1))) {
          const portalCode =
            tile + get(x, y + 1) + (checkIfOuter(x, y) ? '-' : '+');
          if (y + 2 < lines.length && get(x, y + 2) === PATH) {
            portals[portalCode] = { x, y: y + 2 };
          } else if (get(x, y - 1) === PATH) {
            portals[portalCode] = { x, y: y - 1 };
          }
        }
      }
    }
  }

  const portalEdges = {};

  for (const [portalName, locations] of Object.entries(portals)) {
    if (portalName === 'ZZ-') continue;
    portalEdges[portalName] = {};
    for (const portal of Object.keys(portals)) {
      if (portal === portalName) continue;
      const length = findPath(portals[portalName], portals[portal]);
      if (length) {
        portalEdges[portalName][portal] = length;
      }
    }
    if (portalName !== 'AA-' && portalName !== 'ZZ-') {
      const currentSign = portalName[2];
      const newSign = currentSign === '+' ? '-' : '+';
      portalEdges[portalName][portalName.substring(0, 2) + newSign] = 1;
    }
  }

  function findSteps() {
    let bestSteps = Number.MAX_VALUE;
    const checkPaths = [{ path: ['AA-'], length: 0 }];
    const visited = { 'AA-': 0 };

    while (checkPaths.length) {
      const { path, length } = checkPaths.pop();
      if (length > bestSteps) continue;
      const from = path.slice(-1)[0];
      if (from === 'ZZ-') {
        if (length < bestSteps) bestSteps = length;
        continue;
      }
      const possibleLocations = Object.keys(portalEdges[from]);
      for (const to of possibleLocations) {
        const newPath = [...path, to];
        const newLength = length + portalEdges[from][to];
        if (to in visited && visited[to] < length) {
          continue;
        }
        visited[to] = newLength;
        checkPaths.push({ path: newPath, length: newLength });
      }
    }

    return bestSteps;
  }

  function findStepsPlutionianStyle() {
    let bestSteps = Number.MAX_VALUE;
    const checkPaths = [{ path: ['AA-'], level: 0, length: 0 }];
    const visited = { '0AA-': 0 };

    while (checkPaths.length) {
      const { path, length, level } = checkPaths.shift();
      if (length > bestSteps) continue;
      const from = path.slice(-1)[0];
      if (from === 'ZZ-') {
        if (length < bestSteps) bestSteps = length;
        continue;
      }
      const possibleLocations = Object.keys(portalEdges[from]);
      for (const to of possibleLocations) {
        if (
          level === 0 &&
          to[2] === '-' &&
          to !== 'AA-' &&
          to !== 'ZZ-' &&
          from.substring(0, 2) !== to.substring(0, 2)
        )
          continue;
        else if (level !== 0 && (to === 'AA-' || to === 'ZZ-')) {
          continue;
        }
        const newPath = [...path, to];
        const newLength = length + portalEdges[from][to];
        let newLevel = level;
        if (from.substring(0, 2) === to.substring(0, 2)) {
          newLevel += from[2] === '+' ? 1 : -1;
        }
        const visitedKey = `${newLevel}${to}`;
        if (visitedKey in visited && visited[visitedKey] < length) {
          continue;
        }
        visited[visitedKey] = newLength;
        checkPaths.push({ path: newPath, level: newLevel, length: newLength });
      }
    }

    return bestSteps;
  }

  return {
    get,
    portals,
    findSteps,
    findStepsPlutionianStyle,
  };
}

module.exports = {
  part1: inputLines => {
    const { findSteps, portals } = createMaze(inputLines);
    return findSteps();
  },
  part2: inputLines => {
    const { findStepsPlutionianStyle, portals } = createMaze(inputLines);
    return findStepsPlutionianStyle();
  },
};
