const { runProgram } = require('./intcode-computer');

describe('Intcode - OpCode 9', () => {
  it('outputs itself', () => {
    const program = '109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99'
      .split(',')
      .map(v => parseInt(v, 10));
    const ctx = runProgram(program, []);
    expect(ctx.isHalted).toBe(true);
    expect(ctx.output).toEqual(program);
  });

  it('returns number with length 16', () => {
    const program = '1102,34915192,34915192,7,4,7,99,0'
      .split(',')
      .map(v => parseInt(v, 10));
    const ctx = runProgram(program, []);
    expect(ctx.isHalted).toBe(true);
    expect(ctx.output[0].toString().length).toBe(16);
  });

  it('outputs large number in the middle', () => {
    const program = '104,1125899906842624,99'
      .split(',')
      .map(v => parseInt(v, 10));
    const ctx = runProgram(program, []);
    expect(ctx.isHalted).toBe(true);
    expect(ctx.output[0]).toBe(1125899906842624);
  });
});
