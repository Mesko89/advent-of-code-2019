const { modPow, modInv } = require('bigint-crypto-utils');
module.exports = {
  manhattanDistance(p1, p2) {
    return Math.abs(p2.x - p1.x) + Math.abs(p2.y - p1.y);
  },
  permutations(rules) {
    let perms = [[]];
    for (const rule of rules) {
      const nPerm = [];
      for (const item of rule) {
        for (const perm of perms) {
          nPerm.push([...perm, item]);
        }
      }
      perms = nPerm;
    }
    return perms;
  },
  BigInt: { modPow, modInv },
};
