const aStar = require('a-star');
const { manhattanDistance } = require('./utils');

const Tile = {
  Empty: '.',
  Wall: '#',
  Robot: '@',
};

const isKeyRx = /[a-z]/;

function getNeighbors(x, y) {
  return [
    { x: x + 1, y },
    { x: x - 1, y },
    { x, y: y + 1 },
    { x, y: y - 1 },
  ];
}

function createField(lines) {
  const get = (x, y) => lines[y][x];
  const key = pos => `${pos.x},${pos.y}`;
  const keys = {};
  let robots = [];

  const foundPaths = {};

  function findPath(from, to) {
    const fromKey = key(from);
    const toKey = key(to);
    if (fromKey in foundPaths && toKey in foundPaths[fromKey]) {
      return foundPaths[fromKey][toKey];
    }

    const { status, path } = aStar({
      start: from,
      isEnd: ({ x, y }) => x === to.x && y === to.y,
      neighbor: ({ x, y }) =>
        getNeighbors(x, y).filter(({ x, y }) => get(x, y) !== Tile.Wall),
      distance: () => 1,
      heuristic: pos => manhattanDistance(pos, to),
      hash: ({ x, y }) => `${x},${y}`,
    });

    if (!(fromKey in foundPaths)) {
      foundPaths[fromKey] = {};
    }

    foundPaths[fromKey][toKey] =
      status === 'success'
        ? path.map(({ x, y }) => get(x, y)).join('')
        : undefined;

    return foundPaths[fromKey][toKey];
  }

  for (let y = 0; y < lines.length; y++) {
    for (let x = 0; x < lines[y].length; x++) {
      const tile = get(x, y);
      if (isKeyRx.test(tile)) {
        keys[tile] = { x, y };
      } else if (tile === Tile.Robot) {
        robots.push({ x, y });
      }
    }
  }

  return {
    get,
    keys,
    robots,
    findPath,
  };
}

function getShortestLength(field) {
  let shortestPathLength = Number.MAX_VALUE;

  const totalKeys = Object.keys(field.keys).length;
  const shortestPathsForKeys = {};
  field.robots.forEach((_, i) => (shortestPathsForKeys[i] = {}));

  (function checkState(
    currentPositions,
    currentPaths,
    possessedKeys = new Set()
  ) {
    const keysToGet = Object.keys(field.keys).filter(
      k => !possessedKeys.has(k)
    );
    const passableDoors = Array.from(possessedKeys).map(v => v.toUpperCase());
    const canPassRx = new RegExp(`^[${passableDoors.join('')}a-z\.@]+$`, '');

    for (let robotId = 0; robotId < currentPositions.length; robotId++) {
      const currentPosition = currentPositions[robotId];
      const currentPath = currentPaths[robotId];

      for (const keyToGet of keysToGet) {
        let path = field.findPath(currentPosition, field.keys[keyToGet]);
        if (!path) continue;
        path = currentPath.concat(path.substring(1));

        if (!canPassRx.test(path)) {
          continue;
        }

        const positions = [...currentPositions];
        const paths = [...currentPaths.map(v => v.split('').join(''))];
        positions[robotId] = field.keys[path[path.length - 1]];
        paths[robotId] = path.split('').join('');

        const length = paths.reduce((t, p) => t + p.length - 1, 0);

        if (shortestPathLength <= length) {
          continue;
        }

        const keys = new Set();

        for (let robotId = 0; robotId < paths.length; robotId++) {
          for (let i = 0; i < paths[robotId].length; i++) {
            if (paths[robotId][i] in field.keys) {
              keys.add(paths[robotId][i]);
            }
          }
        }

        if (keys.size === totalKeys) {
          if (shortestPathLength > length) shortestPathLength = length;
          continue;
        }

        const foundKeysKey = Array.from(keys)
          .sort()
          .join('');
        if (
          foundKeysKey in shortestPathsForKeys &&
          keyToGet in shortestPathsForKeys[foundKeysKey] &&
          shortestPathsForKeys[foundKeysKey][keyToGet] <= length
        ) {
          continue;
        }
        if (!(foundKeysKey in shortestPathsForKeys))
          shortestPathsForKeys[foundKeysKey] = {};
        shortestPathsForKeys[foundKeysKey][keyToGet] = length;

        checkState(positions, paths, keys);
      }
    }
  })(
    field.robots,
    field.robots.map(() => '@')
  );

  return shortestPathLength;
}

function transform(inputLines) {
  for (let y = 0; y < inputLines.length; y++) {
    for (let x = 0; x < inputLines[y].length; x++) {
      if (inputLines[y][x] === Tile.Robot) {
        inputLines[y - 1] =
          inputLines[y - 1].slice(0, x - 1) +
          Tile.Robot +
          Tile.Wall +
          Tile.Robot +
          inputLines[y - 1].slice(x + 2);
        inputLines[y] =
          inputLines[y].slice(0, x - 1) +
          Tile.Wall +
          Tile.Wall +
          Tile.Wall +
          inputLines[y].slice(x + 2);
        inputLines[y + 1] =
          inputLines[y + 1].slice(0, x - 1) +
          Tile.Robot +
          Tile.Wall +
          Tile.Robot +
          inputLines[y + 1].slice(x + 2);
        return inputLines;
      }
    }
  }
}

module.exports = {
  part1: inputLines => {
    const field = createField(inputLines);
    return getShortestLength(field);
  },
  part2: inputLines => {
    const field = createField(transform(inputLines));
    return getShortestLength(field);
  },
};
