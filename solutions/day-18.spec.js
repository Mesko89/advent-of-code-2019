const { part1, part2 } = require('./day-18');

describe('Day 18 - Part 1', () => {
  const tests = [
    {
      input: ['#########', '#b.A.@.a#', '#########'],
      expectedOutput: 8,
    },
    {
      input: [
        '########################',
        '#f.D.E.e.C.b.A.@.a.B.c.#',
        '######################.#',
        '#d.....................#',
        '########################',
      ],
      expectedOutput: 86,
    },
    {
      input: [
        '########################',
        '#...............b.C.D.f#',
        '#.######################',
        '#.....@.a.B.c.d.A.e.F.g#',
        '########################',
      ],
      expectedOutput: 132,
    },
    {
      input: [
        '########################',
        '#@..............ac.GI.b#',
        '###d#e#f################',
        '###A#B#C################',
        '###g#h#i################',
        '########################',
      ],
      expectedOutput: 81,
    },
    {
      input: [
        '#################',
        '#i.G..c...e..H.p#',
        '########.########',
        '#j.A..b...f..D.o#',
        '########@########',
        '#k.E..a...g..B.n#',
        '########.########',
        '#l.F..d...h..C.m#',
        '#################',
      ],
      expectedOutput: 136,
    },
  ];
  for (const test of tests) {
    it(`returns minimum number of steps to collect all keys "${test.input}"`, () => {
      expect(part1(test.input, test.phases)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 18 - Part 2', () => {
  const tests = [
    {
      input: [
        '#######',
        '#a.#Cd#',
        '##...##',
        '##.@.##',
        '##...##',
        '#cB#.b#',
        '#######',
      ],
      expectedOutput: 8,
    },
    {
      input: [
        '###############',
        '#d.ABC.#.....a#',
        '######...######',
        '######.@.######',
        '######...######',
        '#b.....#.....c#',
        '###############',
      ],
      expectedOutput: 24,
    },
    {
      input: [
        '#############',
        '#DcBa.#.GhKl#',
        '#.###...#I###',
        '#e#d#.@.#j#k#',
        '###C#...###J#',
        '#fEbA.#.FgHi#',
        '#############',
      ],
      expectedOutput: 32,
    },
    {
      input: [
        '#############',
        '#g#f.D#..h#l#',
        '#F###e#E###.#',
        '#dCba...BcIJ#',
        '#####.@.#####',
        '#nK.L...G...#',
        '#M###N#H###.#',
        '#o#m..#i#jk.#',
        '#############',
      ],
      expectedOutput: 72,
    },
  ];
  for (const test of tests) {
    it(`returns minimum number of steps to collect all keys "${test.input}"`, () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
