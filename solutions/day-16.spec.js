const { part1, part2 } = require('./day-16');

describe('Day 16 - Part 1', () => {
  const tests = [
    {
      input: ['12345678'],
      phases: 4,
      expectedOutput: '01029498',
    },
    {
      input: ['80871224585914546619083218645595'],
      phases: 100,
      expectedOutput: '24176176',
    },
    {
      input: ['19617804207202209144916044189917'],
      phases: 100,
      expectedOutput: '73745418',
    },
    {
      input: ['69317163492948606335995924319873'],
      phases: 100,
      expectedOutput: '52432133',
    },
  ];
  for (const test of tests) {
    it(`returns first 8 digits after ${test.phases} phases for "${test.input}"`, () => {
      expect(part1(test.input, test.phases)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 16 - Part 2', () => {
  const tests = [
    {
      input: ['03036732577212944063491565474664'],
      expectedOutput: '84462026',
    },
    {
      input: ['02935109699940807407585447034323'],
      expectedOutput: '78725270',
    },
    {
      input: ['03081770884921959731165446850517'],
      expectedOutput: '53553731',
    },
  ];
  for (const test of tests) {
    it(`returns first 8 digits after ${test.phases} phases for "${test.input}"`, () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
