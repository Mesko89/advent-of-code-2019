const FUEL = 'FUEL';
const ORE = 'ORE';

function parseReactions(inputLines) {
  const creates = {};
  for (const line of inputLines) {
    const [leftSide, rightSide] = line.split(' => ');
    const [totalStr, element] = rightSide.split(' ');
    const output = { element, total: parseInt(totalStr, 10) };
    const input = leftSide.split(', ').reduce((obj, v) => {
      const [totalStr, element] = v.split(' ');
      obj[element] = parseInt(totalStr, 10);
      return obj;
    }, {});
    creates[output.element] = {
      element: output.element,
      totalCreated: output.total,
      needs: input,
    };
  }
  return creates;
}

function calculateOreNeeded(reactions, totalFuel) {
  const fuel = reactions[FUEL];
  fuel.totalCreated = totalFuel;
  for (const element in fuel.needs) {
    fuel.needs[element] *= totalFuel;
  }
  delete reactions[FUEL];
  const reserve = {};

  let steps = 0;
  while (steps++ < 100000) {
    const fuelNeeds = Object.entries(fuel.needs).filter(
      ([element]) => element !== ORE
    );
    if (fuelNeeds.length === 0) break;

    const canProduce = fuelNeeds.filter(
      ([element, total]) => total % reactions[element].totalCreated === 0
    );
    let [element, totalRequired] = canProduce.concat(fuelNeeds)[0];

    // 10 ORE => 10 A
    // 28 A

    if (element in reserve) {
      if (reserve[element] > totalRequired) {
        reserve[element] -= totalRequired;
        totalRequired = 0;
        delete fuel.needs[element];
      } else {
        totalRequired -= reserve[element];
        reserve[element] = 0;
      }
    }

    const needs =
      totalRequired === 0
        ? []
        : Object.entries(
            reactions[element].needs
          ).map(([elementInNeed, total]) => [
            elementInNeed,
            total * Math.ceil(totalRequired / reactions[element].totalCreated),
          ]);

    const overhead =
      Math.ceil(totalRequired / reactions[element].totalCreated) *
        reactions[element].totalCreated -
      totalRequired;

    if (!(element in reserve)) reserve[element] = 0;
    reserve[element] += overhead;

    for (const [element, total] of needs) {
      if (!(element in fuel.needs)) fuel.needs[element] = 0;
      fuel.needs[element] += total;
    }
    delete fuel.needs[element];
  }
  return fuel.needs[ORE];
}

module.exports = {
  part1: inputLines => {
    const reactions = parseReactions(inputLines);
    return calculateOreNeeded(reactions, 1);
  },
  part2: inputLines => {
    let a = 1;
    let b = 10000000000;
    let c = Math.floor((a + b) / 2);
    let oreNeeded = 0;
    do {
      const reactions = parseReactions(inputLines);
      const ore = calculateOreNeeded(reactions, c);
      if (ore > 1000000000000) {
        b = c;
      } else {
        a = c;
        oreNeeded = ore;
      }
      c = Math.floor((a + b) / 2);
    } while (b !== c && a !== c);
    return c;
  },
};
