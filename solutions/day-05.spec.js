const fs = require('fs');
const { part1, part2 } = require('./day-05');
const input = fs
  .readFileSync('./inputs/05.txt', { encoding: 'utf-8' })
  .split(/[\r\n]+/)
  .filter(r => Boolean(r));

describe('Day 05 - Part 1', () => {
  it(`passes all tests`, () => {
    const [last, ...v] = part1(input, true).reverse();
    expect(v.every(v => v === 0)).toBe(true);
    expect(last).not.toBe(0);
  });
});

describe('Day 05 - Part 2', () => {
  const tests = [
    {
      input: ['3,9,8,9,10,9,4,9,99,-1,8'],
      inputValue: [8],
      expectedOutput: [1],
    },
    {
      input: ['3,9,8,9,10,9,4,9,99,-1,8'],
      inputValue: [7],
      expectedOutput: [0],
    },
    {
      input: ['3,9,8,9,10,9,4,9,99,-1,8'],
      inputValue: [9],
      expectedOutput: [0],
    },

    {
      input: ['3,9,7,9,10,9,4,9,99,-1,8'],
      inputValue: [7],
      expectedOutput: [1],
    },
    {
      input: ['3,9,7,9,10,9,4,9,99,-1,8'],
      inputValue: [8],
      expectedOutput: [0],
    },

    { input: ['3,3,1108,-1,8,3,4,3,99'], inputValue: [8], expectedOutput: [1] },
    { input: ['3,3,1108,-1,8,3,4,3,99'], inputValue: [7], expectedOutput: [0] },
    { input: ['3,3,1108,-1,8,3,4,3,99'], inputValue: [9], expectedOutput: [0] },

    { input: ['3,3,1107,-1,8,3,4,3,99'], inputValue: [7], expectedOutput: [1] },
    { input: ['3,3,1107,-1,8,3,4,3,99'], inputValue: [8], expectedOutput: [0] },

    {
      input: ['3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9'],
      inputValue: [0],
      expectedOutput: [0],
    },
    {
      input: ['3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9'],
      inputValue: [3],
      expectedOutput: [1],
    },

    {
      input: ['3,3,1105,-1,9,1101,0,0,12,4,12,99,1'],
      inputValue: [0],
      expectedOutput: [0],
    },
    {
      input: ['3,3,1105,-1,9,1101,0,0,12,4,12,99,1'],
      inputValue: [3],
      expectedOutput: [1],
    },

    {
      input: [
        '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99',
      ],
      inputValue: [7],
      expectedOutput: [999],
    },
    {
      input: [
        '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99',
      ],
      inputValue: [8],
      expectedOutput: [1000],
    },
    {
      input: [
        '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99',
      ],
      inputValue: [9],
      expectedOutput: [1001],
    },
  ];
  for (const test of tests) {
    it(`calculates correct diagnostic code "${test.input}"`, () => {
      expect(part2(test.input, test.inputValue, true)).toEqual(
        test.expectedOutput
      );
    });
  }
});
