const {
  calculateBiodiversity,
  simulateMinute,
  getNeighbors,
  part2,
} = require('./day-24');

describe('Day 24 - Calculate biodiversity', () => {
  const tests = [
    {
      input: ['#....', '.....', '.....', '.....', '.....'],
      expectedOutput: 1,
    },
    {
      input: ['.....', '.....', '.....', '#....', '.#...'],
      expectedOutput: 2129920,
    },
  ];
  for (const test of tests) {
    it(`returns correct biodiversity score for "${test.input}"`, () => {
      expect(calculateBiodiversity(test.input)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 24 - Neighbors', () => {
  const tests = [
    {
      args: [0, 0],
      result: [
        [1, 0],
        [-1, 0],
        [0, 1],
        [0, -1],
      ],
    },
    {
      args: [2, 3],
      result: [
        [3, 3],
        [1, 3],
        [2, 4],
        [2, 2],
      ],
    },
    {
      args: [3, 3, 0],
      result: [
        [2, 3, 0],
        [4, 3, 0],
        [3, 4, 0],
        [3, 2, 0],
      ],
    },
    {
      args: [3, 3, 0],
      result: [
        [2, 3, 0],
        [4, 3, 0],
        [3, 4, 0],
        [3, 2, 0],
      ],
    },
    {
      args: [0, 0, 0],
      result: [
        [2, 1, 1],
        [1, 2, 1],
        [1, 0, 0],
        [0, 1, 0],
      ],
    },
    {
      args: [4, 0, 0],
      result: [
        [2, 1, 1],
        [3, 2, 1],
        [4, 1, 0],
        [3, 0, 0],
      ],
    },
    {
      args: [0, 4, 0],
      result: [
        [1, 2, 1],
        [2, 3, 1],
        [1, 4, 0],
        [0, 3, 0],
      ],
    },
    {
      args: [4, 4, 0],
      result: [
        [3, 2, 1],
        [2, 3, 1],
        [3, 4, 0],
        [4, 3, 0],
      ],
    },
    {
      args: [3, 0, 0],
      result: [
        [2, 1, 1],
        [2, 0, 0],
        [4, 0, 0],
        [3, 1, 0],
      ],
    },
    {
      args: [0, 2, 0],
      result: [
        [1, 2, 1],
        [1, 2, 0],
        [0, 3, 0],
        [0, 1, 0],
      ],
    },
    {
      args: [1, 2, 0],
      result: [
        [0, 2, 0],
        [1, 1, 0],
        [1, 3, 0],
        [0, 0, -1],
        [0, 1, -1],
        [0, 2, -1],
        [0, 3, -1],
        [0, 4, -1],
      ],
    },
    {
      args: [2, 1, 0],
      result: [
        [1, 1, 0],
        [3, 1, 0],
        [2, 0, 0],
        [0, 0, -1],
        [1, 0, -1],
        [2, 0, -1],
        [3, 0, -1],
        [4, 0, -1],
      ],
    },
    {
      args: [3, 2, 0],
      result: [
        [3, 1, 0],
        [3, 3, 0],
        [4, 2, 0],
        [4, 0, -1],
        [4, 1, -1],
        [4, 2, -1],
        [4, 3, -1],
        [4, 4, -1],
      ],
    },
    {
      args: [2, 3, 0],
      result: [
        [3, 3, 0],
        [1, 3, 0],
        [2, 4, 0],
        [0, 4, -1],
        [1, 4, -1],
        [2, 4, -1],
        [3, 4, -1],
        [4, 4, -1],
      ],
    },
  ];

  for (const {
    args: [x, y, layer],
    result,
  } of tests) {
    it(`returns correct neighbors for x=${x}, y=${y}, layer=${layer}`, () => {
      const neighbors = getNeighbors(x, y, layer);
      expect(neighbors.length).toBe(result.length);
      expect(neighbors).toEqual(expect.arrayContaining(result));
    });
  }
});

describe('Day 24 - Simulate', () => {
  const tests = [
    {
      input: ['....#', '#..#.', '#..##', '..#..', '#....'],
      expectedOutput: ['#..#.', '####.', '###.#', '##.##', '.##..'],
    },
    {
      input: ['#..#.', '####.', '###.#', '##.##', '.##..'],
      expectedOutput: ['#####', '....#', '....#', '...#.', '#.###'],
    },
    {
      input: ['#####', '....#', '....#', '...#.', '#.###'],
      expectedOutput: ['#....', '####.', '...##', '#.##.', '.##.#'],
    },
  ];
  for (const test of tests) {
    it(`returns correct state after a minute for "${test.input}"`, () => {
      expect(simulateMinute(test.input)).toEqual(test.expectedOutput);
    });
  }
});

describe('Day 24 - Part 2', () => {
  const tests = [
    {
      input: ['....#', '#..#.', '#..##', '..#..', '#....'],
      expectedOutput: 99,
    },
  ];
  for (const test of tests) {
    it(`returns correct number of bugs after 10 minutes for "${test.input}"`, () => {
      expect(part2(test.input, 10)).toEqual(test.expectedOutput);
    });
  }
});
