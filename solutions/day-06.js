function parseOrbitTree(orbits) {
  const nodeMap = {};

  for (const [A, B] of orbits.map(o => o.split(')'))) {
    if (!(A in nodeMap)) {
      nodeMap[A] = { name: A, parent: null, childList: [] };
    }
    if (!(B in nodeMap)) {
      nodeMap[B] = { name: B, parent: null, childList: [] };
    }
    if (nodeMap[B].parent !== null) {
      console.log({ A, B });
      throw new Error('B already has a parent');
    }
    nodeMap[B].parent = nodeMap[A];
    nodeMap[A].childList.push(nodeMap[B]);
  }
  const nodeList = Object.values(nodeMap);
  return {
    root: nodeList.find(n => n.parent === null),
    you: nodeList.find(n => n.name === 'YOU'),
    santa: nodeList.find(n => n.name === 'SAN'),
  };
}

function countTotalNumberOfOrbits(tree, level = 0) {
  return tree.childList.reduce(
    (t, node) => t + countTotalNumberOfOrbits(node, level + 1),
    level
  );
}

function getPath(node) {
  const path = [];
  while (node.parent) {
    path.push(node.parent);
    node = node.parent;
  }
  return path;
}

function getPathToSanta(you, santa) {
  const youPath = getPath(you);
  const santaPath = getPath(santa);
  const firstCommonNode = youPath.find(nodePath =>
    santaPath.some(n => n === nodePath)
  );
  return [
    ...youPath.slice(0, youPath.indexOf(firstCommonNode)),
    firstCommonNode,
    ...santaPath.slice(0, santaPath.indexOf(firstCommonNode)).reverse(),
  ];
}

module.exports = {
  part1: inputLines => {
    const map = parseOrbitTree(inputLines);
    return countTotalNumberOfOrbits(map.root);
  },
  part2: inputLines => {
    const map = parseOrbitTree(inputLines);
    const path = getPathToSanta(map.you, map.santa);
    return path.length - 1;
  },
};
