const { part1, part2 } = require('./day-04');

describe('Day 04 - Part 1', () => {
  const tests = [{ input: ['111110-111121'], expectedOutput: 9 }];
  for (const test of tests) {
    it(`calculates total number of possible passwords for "${test.input}"`, () => {
      expect(part1(test.input)).toBe(test.expectedOutput);
    });
  }
});

describe('Day 04 - Part 2', () => {
  const tests = [{ input: ['111120-111131'], expectedOutput: 1 }];
  for (const test of tests) {
    it(`calculates total number of better possible passwords for "${test.input}"`, () => {
      expect(part2(test.input)).toBe(test.expectedOutput);
    });
  }
});
